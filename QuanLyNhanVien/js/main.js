var workerList = [];

const workerList_LOCAL = "workerList_LOCAL";
var workerListJson = localStorage.getItem(workerList_LOCAL);
if (workerListJson != null) {
  workerList = JSON.parse(workerListJson);
  for (var index = 0; index < workerList.length; index++) {
    a = workerList[index];
    workerList[index] = new Worker(
      a.account,
      a.name,
      a.email,
      a.password,
      a.day,
      a.slalary,
      a.position,
      a.time
    );
  }
  render(workerList);
}

document.getElementById("btnThemNV").onclick = function addWorker() {
 
  if (!check()) {
    return;
  } else if (checkID("tknv", workerList) == false) {
    Swal.fire("Xin nhập lại ID, ID bạn đã sử dụng");
    return;
  } else {
    var newWorker = info();
    workerList.push(newWorker);
    var workerListJson = JSON.stringify(workerList);
    localStorage.setItem(workerList_LOCAL, workerListJson);
    render(workerList);
    resetInput();
  }
};

function remove(id) {
  var i = find(id, workerList);
  workerList.splice(i, 1);
  var workerListJson = JSON.stringify(workerList);
  localStorage.setItem(workerList_LOCAL, workerListJson);
  render(workerList);
}
function edit(id) {
  document.getElementById("btnCapNhat").style.display = "block";
  document.getElementById("popup").innerText = ``;
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").style.display = "none";
  var i = find(id, workerList);
  var b = workerList[i];
  showInfo(b);
}
document.getElementById("btnThem").onclick = function reset_btn() {
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnThemNV").style.display = "block";

  resetInput();
};

document.getElementById("btnCapNhat").onclick = function update() {
  if (!check()) {
    return;
  } else if (document.getElementById("tknv").disabled != true) {
    Swal.fire("Nhấn thêm để thêm vào danh sách");
    return;
  } else {
    var id = document.getElementById("tknv").value * 1;
    var i = find(id, workerList);
    workerList.splice(i, 1, info());
    var workerListJson = JSON.stringify(workerList);
    localStorage.setItem(workerList_LOCAL, workerListJson);
    render(workerList);
    document.getElementById("popup").innerText = `Đã cập nhật !`;
  }
};

document.getElementById("search").onclick = function name() {
  var a = document.getElementById("xepLoai").value;
  if (a == 2) {
    var b = "xuất sắt";
    renderRank(workerList, b);
  } else if (a == 3) {
    var b = "giỏi";
    renderRank(workerList, b);
  } else if (a == 4) {
    var b = "khá";
    renderRank(workerList, b);
  } else if (a == 5) {
    var b = "trung bình";
    renderRank(workerList, b);
  } else {
    render(workerList);
  }
};
