function Worker(account, name, email, password, day, slalary, position, time) {
  this.account = account;
  this.name = name;
  this.email = email;
  this.password = password;
  this.day = day;
  this.time = time;
  this.slalary = slalary;

  this.slalaryTotal = function () {
    if (position == "Sếp") {
      return slalary * 3;
    } else if (position == "Trưởng phòng") {
      return slalary * 2;
    } else {
      return slalary;
    }
  };
  this.position = position;
  this.timeArray = function () {
    if (time >= 192) {
      return "xuất sắt";
    } else if (time >= 176) {
      return "giỏi";
    } else if (time >= 160) {
      return "khá";
    } else {
      return "trung bình";
    }
  };
}
