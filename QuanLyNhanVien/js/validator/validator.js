function checkTxt(a, id, message) {
  var b = getID(a);
  var letter = new RegExp(
    "^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$"
  );
  if (letter.test(b)) {
    return true;
  } else {
    document.getElementById(id).innerText = message;
    console.log(123);
    return false;
  }
}
function checkEmail(a, id, message) {
  var b = getID(a);
  var mail = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})*$/;
  if (b.match(mail)) {
    return true;
  } else {
    document.getElementById(id).innerText = message;
    return false;
  }
}
function checkPass(a, id, message) {
  var b = getID(a);
  var letter =
    /(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{6,10}$/;
  if (b.match(letter)) {
    return true;
  } else {
    document.getElementById(id).innerText = message;
    return false;
  }
}
function checkLength(txt, max, id, message) {
  var a = txt;
  var c = max;
  if (a.length <= c) {
    return true;
  } else {
    document.getElementById(id).innerText = message;
    return false;
  }
}
function checkID(id, array) {
  var a = getID(id);
  for (var i = 0; i < array.length; i++) {
    if (a == array[i].account) {
      return false;
    } else {
      return true;
    }
  }
}
function checkSalary(value, id, message) {
  if (value > 20000000 || value < 1000000) {
    document.getElementById(id).innerText = message;
    return false;
  } else {
    return true;
  }
}
function checkTime(value, id, message) {
  if (value > 200 || value < 80) {
    document.getElementById(id).innerText = message;
    return false;
  } else {
    return true;
  }
}
var validation = {
  kiemTraRong: function (value, id, message) {
    if (id == "tbChucVu" && value == 1) {
      document.getElementById("tbChucVu").innerText = "Xin chọn chức vụ";
      return false;
    }
    if (value.length == 0) {
      document.getElementById(id).innerText = message;
      return false;
    } else {
      document.getElementById(id).innerText = "";
      return true;
    }
  },
};
function check() {
  var people = info();
  var isValid =
    validation.kiemTraRong(people.account, "tbTKNV", "không để trống") &&
    checkLength(getID("tknv"), 4, "tbTKNV", "Xin nhập lại ID tối đa 4 kí tự");
  isValid =
    isValid &
    (validation.kiemTraRong(people.name, "tbTen", "không để trống") &&
      checkTxt(
        "name",
        "tbTen",
        "Xin nhập lại tên, tên không bao gồm số và các kí tự đặc biệt"
      ));
  isValid =
    isValid &
    (validation.kiemTraRong(people.email, "tbEmail", "không để trống") &&
      checkEmail("email", "tbEmail", "Email không hợp lệ"));
  isValid =
    isValid &
    (validation.kiemTraRong(people.password, "tbMatKhau", "không để trống") &&
      checkPass(
        "password",
        "tbMatKhau",
        "Xin nhập lại Pass, Pass từ 6 đến 10 kí tự bao gồm chứa ít nhất 1 kí tự số,1 kí tự hoa, 1 kí tự đặc biệt"
      ));
  isValid =
    isValid & validation.kiemTraRong(people.day, "tbNgay", "không để trống");
  isValid =
    isValid &
    (validation.kiemTraRong(people.slalary, "tbLuongCB", "không để trống") &&
      checkSalary(
        people.slalary,
        "tbLuongCB",
        "Xin nhập lại lương, khoảng từ 10triệu đến 20 triệu"
      ));
  isValid =
    isValid &
      validation.kiemTraRong(people.time, "tbGiolam", "không để trống") &&
    checkTime(
      people.time,
      "tbGiolam",
      "Xin nhập lại giờ, giờ làm từ 80 đến 200"
    );
  isValid =
    isValid &
    validation.kiemTraRong(people.position, "tbChucVu", "không để trống");
  if (isValid) {
    return true;
  } else {
    Swal.fire("Xin kiểm tra lại dữ liệu");
    return false;
  }
}
