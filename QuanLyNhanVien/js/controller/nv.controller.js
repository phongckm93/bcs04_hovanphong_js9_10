function getID(a) {
  return document.getElementById(a).value;
}

function info() {
  const account = getID("tknv");
  const name = getID("name");
  const email = getID("email");
  const password = getID("password");
  const day = getID("datepicker");
  const slalary = getID("luongCB");
  const position = getID("chucvu");
  const time = getID("gioLam");
  return new Worker(
    account,
    name,
    email,
    password,
    day,
    slalary,
    position,
    time
  );
}
function render(workArr) {
  var content = "";
  for (var i = 0; i < workArr.length; i++) {
    var a = workArr[i];
    var trContent = `<tr>
    <th>
      <span>${a.account}</span>
    </th>
    <th>${a.name}</th>
    <th>${a.email}</th>
    <th>${a.day}</th>
    <th>${a.position}</th>
    <th>${a.slalaryTotal()}</th>
    <th>${a.timeArray()}</th>
    <th class="d-flex"><button type="button" class="btn btn-danger mr-1" onclick="remove(${a.account
      })">Xóa</button>
		<button type="button" class="btn btn-info" data-toggle="modal"
    data-target="#myModal"onclick="edit(${a.account})">Sửa</button></th>
  </tr>`;
    content += trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = content;
}

function find(id, workerList) {
  for (var i = 0; i < workerList.length; i++) {
    if (workerList[i].account == id) {
      return i;
    }
  }
}

function resetInput() {
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("luongCB").value = "";
  document.getElementById("gioLam").value = "";
  document.getElementById("chucvu").value = "1";
  document.getElementById("datepicker").value = "";
  document.getElementById("popup").innerText = ``;
  document.getElementById("xepLoai").value = 1;
}

function showInfo(a) {
  document.getElementById("tknv").value = a.account;
  document.getElementById("name").value = a.name;
  document.getElementById("email").value = a.email;
  document.getElementById("password").value = a.password;
  document.getElementById("luongCB").value = a.slalary;
  document.getElementById("gioLam").value = a.time;
  document.getElementById("chucvu").value = a.position;
  document.getElementById("datepicker").value = a.day;
}

function renderRank(workArr, k) {
  var content = "";
  for (var i = 0; i < workArr.length; i++) {
    var a = workArr[i];
    var b = a.timeArray();
    if (k == b) {
      var trContent = `<tr>
  <th class="nowrap">
    <span class="mr-1">${a.account}</span>
  </th>
  <th>${a.name}</th>
  <th>${a.email}</th>
  <th>${a.day}</th>
  <th>${a.position}</th>
  <th>${a.slalaryTotal()}</th>
  <th>${a.timeArray()}</th>
  <th class="d-flex"><button type="button" class="btn btn-danger mr-1" onclick="remove(${a.account
        })">Xóa</button>
  <button type="button" class="btn btn-info" data-toggle="modal"
  data-target="#myModal"onclick="edit(${a.account})">Sửa</button></th>
</tr>`;
      content += trContent;
    }
    document.getElementById("tableDanhSach").innerHTML = content;
  }
}


document.getElementById("show").onclick = function showPass() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
};
